<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
    <div class="input-group">
        <button class="btn btn-default" type="submit"><i class="fa fa-search fa-fw"></i></button>
    </div><!-- /input-group -->
</form>