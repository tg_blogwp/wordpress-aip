<?php
/*
Template Name:  Home Full Width
 */
$fields = get_fields($post->ID);
$sections = $fields['section_renderer'];

get_header(); 

$s = 1;
    foreach($sections as $section){

        include(locate_template('partials/home-'.$section['acf_fc_layout'].'.php'));
        $s++;

    }

 get_footer(); 
 
 ?>