<?php


echo '<section id="section-'.$s.'" class="graphSection">
            <div class="container-fluid">
                <h2 class="h2">'.$section['titolo_sezione'].'</h2>
                <div class="filetto"></div>
                <div class="row graphs">';



                $p= 1;
               foreach($section['elemento_torta'] as $circle){

                   echo '<div class="circle" id="circles-'.$p.'"></div>';
                
                echo '<style>.circles-text-'.$p.' { color: '.$circle['colore_torta'].' !important; font-size: 40px !important; font-weight: 300 !important}</style>';
                $p++;
               }

echo   '</div>
            </div>
        </section>';



echo  '<script>
jQuery(function(){';

$p= 1;
$dur = 600;
foreach($section['elemento_torta'] as $circle){

    echo "var myCircle = Circles.create({
  id:                  'circles-".$p."',
  radius:              100,
  value:               ".$circle['valore_numerico'].",
  maxValue:            1000,
  width:               5,
  text:                function(value){return value;},
  colors:              ['#fcfcfc', '".$circle['colore_torta']."'],
  duration:            ".$dur.",
  wrpClass:            'circles-wrp',
  textClass:           'circles-text-".$p."',
  valueStrokeClass:    'circles-valueStroke',
  maxValueStrokeClass: 'circles-maxValueStroke',
  styleWrapper:        true,
  styleText:           true
});";
    $dur = $dur+200;
$p++;
}

echo '})
</script>';



?>