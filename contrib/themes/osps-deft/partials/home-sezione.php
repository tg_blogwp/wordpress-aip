<?php

$output = '';
$h = 0;
$b = 0;
$c = 1;
$boxes = '';

foreach($section['categoria_hero'] as $single){

    $cta = '';

    if(trim($single['cta']) != ''){
        $ctaRaw = explode('|',$single['cta']); //<a href="/efficienza-elettronica"><div class="footerCta f-left bgVerde">Finanza e Credito</div></a>
        $cta = '<a href="'.$ctaRaw[1].'"><div class="footerCta" style="background-color:'.$single['colore'].'">'.$ctaRaw[0].'</div></a>';
    }else{

        foreach($single['aree_in_evidenza'] as $area){

            $cta .= '<a href="/'.$area->post_name.'"><div class="footerCta f-left " style="background-color:'.$single['colore'].'">'.$area->post_title.'</div></a>';

        }
    }

    if($single['visualizzazione'] == 'hero'){  //tutta larghezza
        
        $h++;
            if(!is_int($h/2)){
            $content = '<div class="col-md-5 heroCat">
              <div class="heroCatTitle" style="color:'.$single['colore'].'">'.$single['titolo'].'</div>
              <div style="margin:10px 0px">'.$single['descrizione'].'</div>
              <div style="margin:10px 0px; display:inline-flex">'.$cta.'</div>
              </div>
              <div class="col-md-2"></div>
              <div class="col-md-5"></div>';
        }else{
            $content = '<div class="col-md-5"></div>
              <div class="col-md-2"></div>
              <div class="col-md-5 heroCat">
              <div class="heroCatTitle" style="color:'.$single['colore'].'">'.$single['titolo'].'</div>
              <div style="margin:10px 0px" >'.$single['descrizione'].'</div>
              <div style="margin:10px 0px; display:inline-flex">'.$cta.'</div>
              </div>';
        }


        $output .= '<section id="section-category-'.$c.'" style="background:url(\''.$single['immagine']['url'].'\'); background-size:cover; min-height:438px" class="categorySection">
            <div class="container-fluid">
                <div class="row">
                '.$content.'
                </div>
        </section>';

    }else{ //preparo tile
        $b++;
        $boxes .= '<div class="col-md-6 boxCat">
                <div class="boxCatContainer">
              <div class="boxCatImg" style="background:url(\''.$single['immagine']['url'].'\'); background-size:cover;"></div>
              <div class="heroCatTitle" style=" margin:10px 0px; color:'.$single['colore'].'">'.$single['titolo'].'</div>
              <div style="margin:10px 0px" >'.$single['descrizione'].'</div>
              <div style="margin:10px 0px; display:inline-flex" >'.$cta.'</div>
                </div>
        </div>';
    
    }

    $c++;
}


if($b > 0){

    $output .= '<section id="section-category-'.$c.'" class="categoryBoxSection">
            <div class="container-fluid">
                
                '.$boxes.'
                
        </section>';

}

echo $output;
?>