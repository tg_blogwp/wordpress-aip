<?php global $redux_demo; ?>
<?php global $current_user; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri() . '/assets/images/favicon.png' ?>">
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri() . '/assets/images/apple-touch-icon.png' ?>">
        <title><?php wp_title( '|' , 1, 'right' ); ?> <?php bloginfo( 'name' ); ?></title>
        <?php wp_head(); ?>
    </head>

    <body <?php body_class( $class ); ?>>

        <div class="wrapper">

        <div id="mmenu-wrapper"> <!--mmenu wrapper -->
        <!-- Primary Menu-->
        <?php if ( !empty($redux_demo['stage-navbar-style'] ) ) : ?>
            <nav class="navbar <?php echo $redux_demo['stage-navbar-style']; ?> navbar-default" role="navigation">
        <?php else : ?>
            <nav class="navbar navbar-default mm-fixed-top" role="navigation">
        <?php endif; ?>
            <div class="container-fluid">
                <div class="navbar-header">
                    <?php if ( has_nav_menu( 'side-menu' ) ) : ?>
                        <!--mmenu toggle -->
                        <a class="left-navbar-toggle pull-right" href="#menu-left"> <i class="fa fa-bars fa-6x"></i> </a>
                    <?php endif; ?>
                    <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".navbar-collapse">
                  <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand img-responsive" href="<?php echo site_url(); ?>"> <img class="img-responsive" alt=<?=bloginfo( 'name' );?>" src="/contrib/uploads/Logo1.png"/></a>
<!--
                    <a href="#search-modal" data-toggle="modal" class="visible-xs mobile-search"><i class="fa fa-search fa-lg"></i> </a>

--></div>
                <div class="navbar-collapse collapse ">
                    <?php wp_nav_menu(
                        array(
                            'menu'              => 'primary-menu',
                            'theme_location'    => 'primary-menu',
                            'depth'             => 2,
                            'container'         => false,
                            'menu_class'        => 'nav navbar-nav navbar-right',
                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                            'walker'            => new wp_bootstrap_navwalker())
                        );
                    ?>
                     <?php get_search_form(); ?>
                                    </div>
               
            </div>
        </nav>
        <!--Search Modal -->
        <div class="modal fade" id="search-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Search <?php bloginfo( 'name' ); ?></h4>
                    </div>
                    <div class="modal-body">
                        <br/>
                        <?php get_search_form(); ?>
                        <br/><br/>
                    </div>
                </div>
            </div>
        </div>