<?php global $redux_demo; ?>
			</div> <!-- /container -->
			<!--<div class="footer-cta">
				<div class="container-fluid">
						<div class="pull-left"><?php echo $redux_demo['footer-cta-left']; ?></div>

						<div class="pull-right"><?php echo $redux_demo['footer-cta-right']; ?></div>
				</div>
			</div>-->
			<footer>
			<?php if ( has_nav_menu( 'footer-menu' ) ) : ?>
				<div class="footer-menu">

					<div class="container-fluid">
						<div class="pull-left">
						<?php wp_nav_menu( array(
							'menu'              => 'footer-menu',
							'theme_location'    => 'footer-menu',
							'depth'             => 2,
							'container'         => false,
							'container_class'   => 'navbar',
							'menu_class'        => 'nav nav-pills',
							'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
							'walker'            => new wp_bootstrap_navwalker())
						); ?>
						</div>
					<div class="pull-right">
						<ul class="social list-inline pull-left">
							<li><a href="https://www.facebook.com/oneinternetfreeline?ref=hl" class="pure-button socicon-button"><span class="socicon socicon-facebook"></span></a></li>
							<li><a href="https://twitter.com/Wispone" class="pure-button socicon-button"><span class="socicon socicon-twitter"></span></a></li>
							<li><a href="#" class="pure-button socicon-button"><span class="socicon socicon-linkedin"></span></a></li>
							<li><a href="#" class="pure-button socicon-button"><span class="socicon socicon-youtube"></span></a></li>
							<li><a href="#" class="pure-button socicon-button"><span class="socicon socicon-skype"></span></a></li>

						</ul>
			        </div>


				</div>
			<?php endif; ?>
				<div class="footer-widgets-panel">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-3"><?php dynamic_sidebar( 'footer-column-1' ); ?></div>
							<div class="col-md-6"><?php dynamic_sidebar( 'footer-column-2' ); ?></div>
							<!--<div class="col-md-3"><?php dynamic_sidebar( 'footer-column-3' ); ?></div>-->
							<div class="col-md-3"><?php dynamic_sidebar( 'footer-column-3' ); ?></div>
						</div>
					</div>
				</div>
				<div class="footer-credits">
					<div class="container-fluid">
						<h6 class="pull-left"></h6>
						<div class="pull-right"><a title="Digital Marketing" target="_blank" href="http://www.websolute.com">
<img src="<?php echo get_template_directory_uri() . '/assets/images/DigitalMarketing.svg'?>" alt="Digital Marketing" width="15" height="15" style="vertical-align: middle;"> websolute
</a></div>
					</div>
				</div>
			</footer>
			<?php wp_footer(); ?>
			<script type="text/javascript"><?php echo $redux_demo['tracking-code']; ?></script>
		</div> <!-- /mmenu wrapper -->
    </div> <!-- /wrapper -->
	</body>
</html>